///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file nene.hpp
/// @version 1.0
///
/// Exports data about all nene
///
/// @author Drew Watanabe <drewwata@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   27_Feb_2021
///////////////////////////////////////////////////////////////////////////////

#pragma once 

#include <string>

#include "bird.hpp" 

using namespace std;


namespace animalfarm {

   class Nene : public Bird {

      public:
         string id;

         Nene( string tagID, enum Color newColor, enum Gender newGender );

         virtual const string speak();

         void printInfo();

   };

} // namespace animalfarm
